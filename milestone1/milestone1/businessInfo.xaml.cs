﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Npgsql;

namespace milestone1
{
    /// <summary>
    /// Interaction logic for businessInfo.xaml
    /// </summary>
    public partial class businessInfo : Window
    {
        string extern_business_id = "";
        public businessInfo()
        {
            InitializeComponent();
            addColumns2Grid();
        }

        public void crosstalk(string phone)
        {
            extern_business_id = phone;
        }

        private string buildConnectionString()
        {
            return "Host = api.jpope.dev; username = genericdb_api; Database = genericdb; password= Q01s$wkC&h$qwmy8";
        }

        private void addColumns2Grid()
        {
            DataGridTextColumn col1 = new DataGridTextColumn();
            col1.Header = "Tip";
            col1.Binding = new Binding("tip");
            col1.Width = 250;
            TipGrid.Columns.Add(col1);

            DataGridTextColumn col2 = new DataGridTextColumn();
            col2.Header = "Likes";
            col2.Binding = new Binding("likes");
            col2.Width = 60;
            TipGrid.Columns.Add(col2);

            DataGridTextColumn col3 = new DataGridTextColumn();
            col3.Header = "Date";
            col3.Binding = new Binding("date");
            col3.Width = 150;
            TipGrid.Columns.Add(col3);
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            using (var connection = new NpgsqlConnection(buildConnectionString()))  // Display all businesses from selected zip code on data grid
            {
                connection.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = "INSERT INTO \"Tips\"(business_id, user_id, date, likes, text) VALUES ('" + extern_business_id + "', 'wzmwID9z8EdRNEDrKk2DHQ', '" + DateTime.Now.ToString() + "', '0', '" + TestBox.Text.ToString() + "')";
                    try
                    {   
                        var reader = cmd.ExecuteReader();
                    }
                    catch (NpgsqlException ex)
                    {
                        Console.WriteLine(ex.Message.ToString());
                        System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            AddCheckLabel.IsChecked = true;
            AddCheckLabel.Visibility = System.Windows.Visibility.Visible;
            return;
        }
    }
}
