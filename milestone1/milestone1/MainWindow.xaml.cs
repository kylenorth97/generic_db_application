﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;
using System.Data.SqlClient;
using System.Data;

namespace milestone1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int stateBusinessNum = 0;
        int IDNum = 0;
        int CategoryNum = 0;
        string business_id = "";
        public class Business
        {
            public string name { get; set; }
            public string state { get; set; }
            public string city { get; set; }
            public string id { get; set; }
            public string postal_code { get; set; }
            public string business_category { get; set; }

        }
        public class TipChart
        {
            public string tip { get; set; }
            public int likes { get; set; }
            public DateTime date { get; set; }

        }
        public MainWindow()
        {
            InitializeComponent();
            addState();
            addColumns2Grid();
        }

        private string buildConnectionString()
        {
            return "Host = api.jpope.dev; username = genericdb_api; Database = genericdb; password= Q01s$wkC&h$qwmy8";
        }

        private void addState()
        {
            using (var connection = new NpgsqlConnection(buildConnectionString()))
            {
                connection.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = "select distinct state from \"Businesses\" order by state";
                    try
                    {
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                            StateList.Items.Add(reader.GetString(0));
                    }
                    catch (NpgsqlException ex)
                    {
                        Console.WriteLine(ex.Message.ToString());
                        System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

        }
        private void addColumns2Grid()
        {
            DataGridTextColumn col1 = new DataGridTextColumn();
            col1.Header = "BusinessName";
            col1.Binding = new Binding("name");
            col1.Width = 225;
            BusyGrid.Columns.Add(col1);

            DataGridTextColumn col2 = new DataGridTextColumn();
            col2.Header = "State";
            col2.Binding = new Binding("state");
            col2.Width = 60;
            BusyGrid.Columns.Add(col2);

            DataGridTextColumn col3 = new DataGridTextColumn();
            col3.Header = "City";
            col3.Binding = new Binding("city");
            col3.Width = 150;
            BusyGrid.Columns.Add(col3);
        }
        private void StateList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CityList.Items.Clear();
            stateBusinessNum = 0;
            if (StateList.SelectedIndex > -1)
            {
                using (var connection = new NpgsqlConnection(buildConnectionString()))
                {
                    connection.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = connection;
                        cmd.CommandText = "SELECT distinct city FROM \"Businesses\" WHERE state = '" + StateList.SelectedItem.ToString() + "' ORDER BY city";
                        try
                        {
                            var reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                CityList.Items.Add(reader.GetString(0));
                                stateBusinessNum++;
                            }
                        }
                        catch (NpgsqlException ex)
                        {
                            Console.WriteLine(ex.Message.ToString());
                            System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }

            }
        }

        private void CityList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BusyGrid.Items.Clear();
            ZipList.Items.Clear();
            if (CityList.SelectedIndex > -1)
            {
                using (var connection = new NpgsqlConnection(buildConnectionString()))
                {
                    connection.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = connection;
                        cmd.CommandText = "SELECT distinct postal_code FROM \"Businesses\" WHERE city = '" + CityList.SelectedItem.ToString() + "' ORDER BY postal_code";
                        try
                        {
                            var reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                ZipList.Items.Add(reader.GetString(0));
                            }
                        }
                        catch (NpgsqlException ex)
                        {
                            Console.WriteLine(ex.Message.ToString());
                            System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }

            }
        }

        private void ZipList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BusyGrid.Items.Clear();
            IDList.Items.Clear();
            CategoriesIDList.Items.Clear();
            CategoriesList.Items.Clear();
            int itr = 0;
            IDNum = 0;
            CategoryNum = 0;
            if (ZipList.SelectedIndex > -1)
            {
                using (var connection = new NpgsqlConnection(buildConnectionString()))  // Display all businesses from selected zip code on data grid
                {
                    connection.Open();
                    using (var cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = connection;
                        cmd.CommandText = "SELECT distinct name, state, city, id FROM \"Businesses\" WHERE state = '" + StateList.SelectedItem.ToString() + "' AND city = '" + CityList.SelectedItem.ToString() + "' AND postal_code = '" + ZipList.SelectedItem.ToString() + "' ORDER BY name;";
                        try
                        {
                            var reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                BusyGrid.Items.Add(new Business() { name = reader.GetString(0), state = reader.GetString(1), city = reader.GetString(2), id = reader.GetString(3) });
                                IDList.Items.Add(reader.GetString(3));
                                IDNum++;
                            }
                        }
                        catch (NpgsqlException ex)
                        {
                            Console.WriteLine(ex.Message.ToString());
                            System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }

                using (var connection = new NpgsqlConnection(buildConnectionString()))  // Display all businesses from selected zip code on data grid
                {
                    using (var cmd = new NpgsqlCommand())
                    {
                        try
                        {
                            itr = 0;
                            while (itr < IDNum)       // Select catagory ID's based on the business ID's
                            {
                                connection.Open();
                                cmd.Connection = connection;
                                cmd.CommandText = "SELECT distinct category_id FROM \"Categories\" WHERE business_id = '" + IDList.Items.GetItemAt(itr).ToString() + "' ";
                                var reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    if (!CategoriesIDList.Items.Contains(reader.GetInt16(0)))
                                    {
                                        CategoriesIDList.Items.Add(reader.GetInt16(0));
                                        CategoryNum++;
                                    }
                                }
                                itr++;
                                connection.Close();
                            }
                        }
                        catch (NpgsqlException ex)
                        {
                            Console.WriteLine(ex.Message.ToString());
                            System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }

                using (var connection = new NpgsqlConnection(buildConnectionString()))  // Display all businesses from selected zip code on data grid
                {
                    using (var cmd = new NpgsqlCommand())
                    {
                        try
                        {
                            itr = 0;
                            while (itr < CategoryNum)    // Place all of the categories into a list based on the category ID's
                            {
                                connection.Open();
                                cmd.Connection = connection;
                                cmd.CommandText = "SELECT distinct name FROM meta.\"MetadataCategories\" WHERE id = '" + CategoriesIDList.Items.GetItemAt(itr) + "' ORDER BY name";
                                var reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    if (!CategoriesList.Items.Contains(reader.GetString(0)))
                                        CategoriesList.Items.Add(reader.GetString(0));
                                }

                                connection.Close();
                                itr++;
                            }
                        }
                        catch (NpgsqlException ex)
                        {
                            Console.WriteLine(ex.Message.ToString());
                            System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }

            }
        }

        private void CategoryList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BusyGrid.Items.Clear();
            FilteredIDList.Items.Clear();
            FilteredBusinessList.Items.Clear();
            int itr = 0;
            int test = CategoriesList.SelectedIndex;
            if (CategoriesList.SelectedIndex > -1)
            {
                using (var connection = new NpgsqlConnection(buildConnectionString()))  
                {
                    using (var cmd = new NpgsqlCommand())
                    {
                        try
                        {
                            foreach (var item in CategoriesList.SelectedItems)   // Select business ID's that correlate to the selected categories
                            {
                                connection.Open();
                                cmd.Connection = connection;
                                cmd.CommandText = "SELECT business_id FROM \"Categories\" WHERE category_id IN (SELECT distinct id FROM meta.\"MetadataCategories\" WHERE name = '" + item + "') ORDER BY business_id "; // + "' AND state = '" + StateList.SelectedItem.ToString() + "' AND city = '" + CityList.SelectedItem.ToString() + "' AND postal_code = '" + ZipList.SelectedItem.ToString() + "' ORDER BY business_id"
                                var reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    if (!FilteredBusinessList.Items.Contains(reader.GetString(0)))
                                        FilteredBusinessList.Items.Add(reader.GetString(0));
                                }
                                connection.Close();
                            }
                        }
                        catch (NpgsqlException ex)
                        {
                            Console.WriteLine(ex.Message.ToString());
                            System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }

                using (var connection = new NpgsqlConnection(buildConnectionString())) 
                {
                    using (var cmd = new NpgsqlCommand())
                    {
                        try
                        {
                            itr = 0;
                            while (itr < FilteredBusinessList.Items.Count)    // Populate the grid with all the filtered businesses
                            {
                                if (IDList.Items.Contains(FilteredBusinessList.Items.GetItemAt(itr).ToString()))
                                {
                                    connection.Open();
                                    cmd.Connection = connection;
                                    cmd.CommandText = "SELECT distinct name, state, city, id FROM \"Businesses\" WHERE id = '" + FilteredBusinessList.Items.GetItemAt(itr).ToString() + "' ORDER BY name";
                                    var reader = cmd.ExecuteReader();
                                    while (reader.Read())
                                    {
                                        BusyGrid.Items.Add(new Business() { name = reader.GetString(0), state = reader.GetString(1), city = reader.GetString(2), id = reader.GetString(3)});
                                    }

                                    connection.Close();

                                }
                                itr++;
                            }
                        }
                        catch (NpgsqlException ex)
                        {
                            Console.WriteLine(ex.Message.ToString());
                            System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
            }
        }

        private void BusyGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int numWords = 0;
            int wrapText = 0;
            FilteredTipList.Items.Clear();
            Dictionary<string, int> tipHash = new Dictionary<string, int>();

            if (BusyGrid.SelectedIndex > -1)
            {
                businessInfo businessInfoDialog = new businessInfo();
                Business selectedBusiness = (Business)BusyGrid.SelectedItem;
                businessInfoDialog.TipGrid.Items.Clear();

                if(selectedBusiness.name != null)
                {
                    using (var connection = new NpgsqlConnection(buildConnectionString()))  // Gather tip data
                    {
                        connection.Open();
                        using (var cmd = new NpgsqlCommand())
                        {
                            cmd.Connection = connection;
                            cmd.CommandText = "SELECT text, likes, date FROM \"Tips\" WHERE business_id = '" + selectedBusiness.id.ToString() + "' ORDER BY likes DESC"; // This index is temporary until I can distingish exactly which business_id was selected in the case of duplicates
                            try
                            {
                                var reader = cmd.ExecuteReader();
                                while (reader.Read())
                                {
                                    string[] tipString = reader.GetString(0).ToString().Split(' ');
                                    string wrappedTip = "";
                                    numWords = tipString.Count();
                                    while(wrapText < numWords)
                                    {
                                        if((wrapText % 7) == 0 && wrapText != 0)
                                        {
                                            wrappedTip += '\n';
                                        }

                                        wrappedTip += tipString[wrapText] + ' ';
                                        wrapText++;
                                    }
                                    business_id = selectedBusiness.id.ToString();
                                    wrapText = 0;
                                    businessInfoDialog.TipGrid.Items.Add(new TipChart() { tip = wrappedTip, likes = reader.GetInt16(1), date = reader.GetDateTime(2) });
                                }
                            }
                            catch (NpgsqlException ex)
                            {
                                Console.WriteLine(ex.Message.ToString());
                                System.Windows.MessageBox.Show("SQL Error - " + ex.Message.ToString());

                            }
                            finally
                            {
                                connection.Close();
                            }
                        }
                    }

                    businessInfoDialog.BusinessName.AppendText(selectedBusiness.name.ToString());
                    businessInfoDialog.BusinessState.AppendText(StateList.SelectedItem.ToString());
                    businessInfoDialog.BusinessCity.AppendText(CityList.SelectedItem.ToString());

                    businessInfoDialog.crosstalk(business_id);
                    businessInfoDialog.Show();
                }
            }
        }

    }
}
